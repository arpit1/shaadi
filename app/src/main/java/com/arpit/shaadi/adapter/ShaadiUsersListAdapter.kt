package com.arpit.shaadi.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.arpit.shaadi.R
import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.databinding.RowLayoutCardBinding
import com.arpit.shaadi.ui.home.HomeViewModel

class ShaadiUsersListAdapter (
    var cards: ArrayList<ShaadiCards>,
    private var viewModel: HomeViewModel
) : RecyclerView.Adapter<ShaadiUsersListAdapter.AreaListViewHolder>() {

    override fun getItemCount() = cards.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AreaListViewHolder(
            DataBindingUtil.inflate (
                LayoutInflater.from(parent.context), R.layout.row_layout_card, parent, false
            )
        )

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: AreaListViewHolder, position: Int) {
        holder.rowLayoutCardBinding.repo = cards[position]
        holder.rowLayoutCardBinding.viewModel = viewModel
    }

    inner class AreaListViewHolder(
        val rowLayoutCardBinding: RowLayoutCardBinding
    ) : RecyclerView.ViewHolder(rowLayoutCardBinding.root)
}