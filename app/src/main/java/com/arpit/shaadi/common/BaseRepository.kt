package com.arpit.shaadi.common

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.arpit.shaadi.MyApplication
import retrofit2.Response
import java.io.IOException
import com.arpit.shaadi.data.model.Result
import com.arpit.shaadi.util.NoInternetException

open class BaseRepository {

    private val isNetworkConnected: Boolean
        get() {
            val cm = MyApplication.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {
        if (!isNetworkConnected) {
            throw NoInternetException("Please check internet connection.")
        }
        val result: Result<T> = safeApiResult(call, errorMessage)
        var data: T? = null

        when (result) {
            is Result.Success ->
                data = result.data
            is Result.Error -> {
                Log.d("DataRepository", "$errorMessage & Exception - ${result.exception}")
            }
        }

        return data
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): Result<T> {
        val response = call.invoke()
        if (response.isSuccessful) return Result.Success(response.body()!!)
        if (response.code() == 400) return Result.Error(Exception("Something went wrong try again later"))
        if (response.code() == 500) return Result.Error(Exception("Something went wrong try again later"))

        return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }
}