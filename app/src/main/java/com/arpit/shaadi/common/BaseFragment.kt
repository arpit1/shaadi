package com.arpit.shaadi.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.arpit.shaadi.R
import com.arpit.shaadi.notifiers.Loader
import com.arpit.shaadi.notifiers.Notify
import com.arpit.shaadi.notifiers.NotifyException
import com.arpit.shaadi.notifiers.NotifyRetry
import com.arpit.shaadi.util.Utility
import com.arpit.shaadi.util.showErrorSnackBar
import com.arpit.shaadi.util.showSnackBarWithRetry
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

abstract class BaseFragment: Fragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var baseBinding: ViewDataBinding

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val activity = context as BaseActivity
        if (!dataBinding) {
            inflater.inflate(layoutResource, container, false)
        } else {
            baseBinding = DataBindingUtil.inflate(inflater, layoutResource, container,false)
        }
        initializeViewModel()
        setBindings()

        getViewModel()?.let {
            it.notifier.recieve { event ->
                when (event) {
                    is NotifyException -> {
                        event.exception.message?.let { msg ->
                            activity.showErrorSnackBar(getBinding().root, msg)
                        }
                    }
                    is Loader -> {
                        if (event.loading) {
                            Utility.showProgressDialog(getActivity() as BaseActivity, true)
                        } else {
                            Utility.hideProgressDialog()
                        }
                    }
                    is NotifyRetry -> {
                        getBinding().root.showSnackBarWithRetry(getString(R.string.text_check_internet), event.call)
                    }
                    else -> {
                        onNotificationReceived(event)
                    }
                }
            }
        }
        return baseBinding.root
    }

    fun getBinding(): ViewDataBinding {
        return baseBinding
    }
    abstract val dataBinding: Boolean
    abstract val layoutResource: Int
    abstract fun getViewModel(): BaseViewModel?
    abstract fun onNotificationReceived(data: Notify)
    abstract fun setBindings()
    abstract fun initializeViewModel()

    inline fun <reified T> lazyBinding(): Lazy<T> = lazy { getBinding() as T }
}