package com.arpit.shaadi.util

import androidx.room.TypeConverter
import com.arpit.shaadi.data.db.entities.Age
import com.arpit.shaadi.data.db.entities.Location
import com.arpit.shaadi.data.db.entities.Name
import com.arpit.shaadi.data.db.entities.Picture
import com.google.gson.Gson

open class Converters {
    @TypeConverter
    fun getRepoObjectJson(value: String): Age? {
        val gson = Gson()
        return gson.fromJson(value, Age::class.java)
    }

    @TypeConverter
    fun setRepoObject(repoObj: Age?): String {
        val gson = Gson()
        return gson.toJson(repoObj)
    }
}

open class LocationConverter {
    @TypeConverter
    fun setRepoObject(repoObj: Location?): String {
        val gson = Gson()
        return gson.toJson(repoObj)
    }

    @TypeConverter
    fun getRepoObjectJson(value: String): Location? {
        val gson = Gson()
        return gson.fromJson(value, Location::class.java)
    }
}

open class NameConverter {
    @TypeConverter
    fun setRepoObject(repoObj: Name?): String {
        val gson = Gson()
        return gson.toJson(repoObj)
    }

    @TypeConverter
    fun getRepoObjectJson(value: String): Name? {
        val gson = Gson()
        return gson.fromJson(value, Name::class.java)
    }
}

open class PictureConverter {
    @TypeConverter
    fun setRepoObject(repoObj: Picture?): String {
        val gson = Gson()
        return gson.toJson(repoObj)
    }

    @TypeConverter
    fun getRepoObjectJson(value: String): Picture? {
        val gson = Gson()
        return gson.fromJson(value, Picture::class.java)
    }
}