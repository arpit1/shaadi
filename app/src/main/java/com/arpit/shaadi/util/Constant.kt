package com.arpit.shaadi.util

class Constant {
    companion object {
        const val ON_ERROR = "ON_ERROR"
        const val NO_INTERNET = "No Internet"
        const val RETRY = "RETRY"
        const val NO_INTERNET_CONNECTION_ERROR = "Uh, oh! It seems like you are not connected to the internet. Please connect to a network and try again."
    }
}