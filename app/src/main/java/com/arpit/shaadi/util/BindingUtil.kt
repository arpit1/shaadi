package com.arpit.shaadi.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.arpit.shaadi.R
import com.arpit.shaadi.data.db.entities.Location
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

object BindingUtil {

    @BindingAdapter("imageUrl", "errorDrawable", "circular")
    @JvmStatic
    fun setImage(
        imageView: ImageView,
        url: String?,
        errorDrawable: Drawable?,
        circular: Boolean = false
    ) {
        var errorDrawableImage = errorDrawable

        if (url == null || url.isEmpty()) {
            return
        }

        if (errorDrawableImage == null) {
            errorDrawableImage =
                ContextCompat.getDrawable(
                    imageView.context,
                    R.drawable.ic_image_placeholder_wrapper
                )
        }

        if (circular)
            Picasso.get().load(url).networkPolicy(NetworkPolicy.NO_CACHE)
                .error(errorDrawableImage!!).transform(CircleTransform()).into(imageView)
        else
            Picasso.get().load(url).networkPolicy(NetworkPolicy.NO_CACHE)
                .error(errorDrawableImage!!).into(imageView)
    }

    @BindingAdapter("title", "first", "last")
    @JvmStatic
    fun setText(
        textView: TextView,
        first: String?,
        last: String?,
        title: String
    ) {
        val name = "$title $first $last"
        textView.text = name
    }

    @BindingAdapter("location")
    @JvmStatic
    fun setLocation(
        textView: TextView,
        location: Location
    ) {
        val loc = "${location.city}, ${location.state}, ${location.country}"
        textView.text = loc
    }

    @BindingAdapter("age")
    @JvmStatic
    fun setAge(
        textView: TextView,
        age: String
    ) {
        textView.text = "$age years"
    }
}