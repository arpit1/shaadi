package com.arpit.shaadi.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.arpit.shaadi.MyApplication
import com.arpit.shaadi.R
import com.arpit.shaadi.data.contract.ErrorActionListener
import com.arpit.shaadi.data.model.ErrorModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.showErrorSnackBar(view: View?, msg: String) {
    try {
        if (view != null) {
            val snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_SHORT)
                .setAction("RETRY", null)
                .setBackgroundTint(ContextCompat.getColor(this, R.color.black))

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<TextView>(R.id.snackbar_text)

            textView.setTextColor(ContextCompat.getColor(this, R.color.white))
            snackbar.show()
        }
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun View.showSnackBarWithRetry(msg: String, callBack: () -> Unit) {
    try {
        val snackbar = Snackbar.make(this, msg, Snackbar.LENGTH_INDEFINITE)
            .setAction("RETRY") {
                callBack.invoke()
            }
            .setActionTextColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
            .setBackgroundTint(ContextCompat.getColor(this.context, R.color.black))
            .setTextColor(ContextCompat.getColor(this.context, R.color.white))
            .setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE)
        val button = snackbar.view.findViewById<MaterialButton>(R.id.snackbar_action)
        button.setRippleColorResource(R.color.black)
        snackbar.show()
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun Exception.logOnCrashAnalytics() {
    this.printStackTrace()
}

fun isInternetAvailable(): Boolean {
    var result = false
    val connectivityManager = MyApplication.getInstance()
        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

    connectivityManager?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    else -> false
                }
            }
        } else {
            connectivityManager.activeNetworkInfo.also { networkInfo ->
                return networkInfo != null && networkInfo.isConnected
            }
        }
    }
    return result
}

fun ErrorModel.showErrorModel(
    title: String,
    subTitle: String,
    button: String,
    action: () -> Unit
) {
    this.apply {
        errorTitle = title
        errorSubTitle = subTitle
        buttonText = button
        errorActionListener = object : ErrorActionListener {
            override fun onErrorActionClicked() {
                action()
            }
        }
    }
}