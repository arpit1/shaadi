package com.arpit.shaadi.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.arpit.shaadi.data.db.dao.ShaadiDao
import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.util.Converters
import com.arpit.shaadi.util.LocationConverter
import com.arpit.shaadi.util.NameConverter
import com.arpit.shaadi.util.PictureConverter

@Database(
    entities = [ShaadiCards::class],
    version = 1
)

@TypeConverters(Converters::class, LocationConverter::class, NameConverter::class, PictureConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getShaadiDao(): ShaadiDao

    companion object {
        //    @Volatile is used so that this variable is immediately visible to all other threads
        @Volatile
        private var instance: AppDatabase? = null
        //    it is use to check that two instances not get created for database
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "Shaadi.db"
            ).build()
    }
}