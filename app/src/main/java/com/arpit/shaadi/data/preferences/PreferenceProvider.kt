package com.arpit.shaadi.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.collections.ArrayList

class PreferenceProvider(var context: Context) {
    private var PRIVATE_MODE = 0
    private val PREFERENCE_NAME = "shaadi"
    private val preference: SharedPreferences
        get() =
            context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE)

    fun saveData(key: String, value: Boolean = false) {
        preference.edit().putBoolean(key, value).apply()
    }

    fun getData(key: String): Boolean? {
        return preference.getBoolean(key, false)
    }

    fun saveCardsJsonObject(myObject: ArrayList<String?>) {
        val prefsEditor = preference.edit()
        val gson = Gson()
        val json = gson.toJson(myObject)
        prefsEditor.putString("MyObject", json)
        prefsEditor.apply()
    }

    fun getCardsJsonObject(): ArrayList<String?>? {
        val gson = Gson()
        val json = preference.getString("MyObject", "")
        val type: Type =
            object : TypeToken<List<String?>?>() {}.type
        return gson.fromJson(json, type)
    }
}