package com.arpit.shaadi.data.contract

interface ErrorActionListener {
    fun onErrorActionClicked()
}