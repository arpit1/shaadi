package com.arpit.shaadi.data.db.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.arpit.shaadi.util.Converters
import com.arpit.shaadi.util.LocationConverter
import com.arpit.shaadi.util.NameConverter
import com.arpit.shaadi.util.PictureConverter

@Entity
data class ShaadiCards(
    @NonNull
    @PrimaryKey
    var email: String,
    var gender: String? = null,
    var phone: String? = null,
    var cell: String? = null,
    var nat: String? = null,
    var status: Boolean? = false,
    var accepted: Boolean? = false,
    var rejected: Boolean? = false,
    @TypeConverters(NameConverter::class)
    var name: Name? = null,
    @TypeConverters(LocationConverter::class)
    var location: Location? = null,
    @TypeConverters(Converters::class)
    var dob: Age? = null,
    @TypeConverters(PictureConverter::class)
    var picture: Picture? = null
)

data class Name(
    var title: String,
    var first: String,
    var last: String
)

data class Location(
    var city: String,
    var state: String,
    var country: String
)

data class Picture(
    var large: String,
    var medium: String,
    var thumbnail: String
)

data class Age(
    var age: String
)