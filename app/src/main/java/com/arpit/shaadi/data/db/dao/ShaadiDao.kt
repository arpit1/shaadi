package com.arpit.shaadi.data.db.dao

import androidx.room.*
import com.arpit.shaadi.data.db.entities.ShaadiCards

@Dao
interface ShaadiDao {

    @Transaction
    suspend fun updateData(shaadiCards: List<ShaadiCards>) {
        delete()
        insert(shaadiCards)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(repo: List<ShaadiCards>)

    @Query("DELETE FROM ShaadiCards")
    suspend fun delete()

    @Query("SELECT * FROM ShaadiCards")
    suspend fun getAllShaadiCards(): List<ShaadiCards>
}