package com.arpit.shaadi.data.db.repository

import com.arpit.shaadi.data.db.AppDatabase
import com.arpit.shaadi.data.db.entities.ShaadiCards

class ShaadiCardRepository(private val database: AppDatabase) {

    suspend fun saveRepo(data: List<ShaadiCards>) = database.getShaadiDao().updateData(data)

    suspend fun getRepo() = database.getShaadiDao().getAllShaadiCards()
}