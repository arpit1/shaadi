package com.arpit.shaadi.repository

import com.arpit.shaadi.common.BaseRepository
import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.network.FormService
import com.arpit.shaadi.data.model.Result
import com.arpit.shaadi.data.model.ServerResponse

class HomeDataRepository(private val formService: FormService): BaseRepository() {

    suspend fun getCards(): Result<ServerResponse<ShaadiCards>>? {
        val response = safeApiCall(
            call = {
                formService.getCards(result = 10)
            },
            errorMessage = "Failed to fetch Details"
        )

        return response?.let {
            Result.Success(it)
        }
    }
}