package com.arpit.shaadi.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.arpit.shaadi.R

class CustomButton(context: Context, attrs: AttributeSet) :
    AppCompatButton(context, attrs) {
    init {
        val face = Typeface.createFromAsset(context.assets, "fonts/SourceSansPro_Regular.otf")
        this.typeface = face
        this.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(R.dimen.text_size_medium)
        )
        this.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        this.setTextColor(ContextCompat.getColor(context, R.color.white))
        this.setPadding(300, 10, 300, 10)
    }
}