package com.arpit.shaadi.network

import android.util.Log
import com.arpit.shaadi.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule {

    /*@GET("random")
    suspend fun getHomeData(): Response<HomeData>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): NetworkModule {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://dog.ceo/api/breeds/image/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(NetworkModule::class.java)
        }
    }*/

    private val retrofitInstance: Retrofit = Retrofit.Builder()
        .client(
            OkHttpClient.Builder().apply {
                addInterceptor(
                    HttpLoggingInterceptor { message ->
                        val maxLogSize = 1000
                        for (i in 0..message.length / maxLogSize) {
                            val start = i * maxLogSize
                            var end = (i + 1) * maxLogSize
                            end = if (end > message.length) message.length else end
                            Log.d("Network Intercept", message.substring(start, end))
                        }
                    }.apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                )
                addInterceptor { chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url

                    /**
                     * Common Query Params can be added over here
                     */
                    val url = originalHttpUrl.newBuilder()
                        .addQueryParameter("platform", "android")
                        .build()

                    /**
                     * Header can be added from here.
                     */
                    val request = original.newBuilder()
                        .header("app_name", "Habbit")
                        .url(url)
                        .build()


                    chain.proceed(request)
                }
            }.build()
        )
        .baseUrl(BuildConfig.baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val formService: FormService = retrofitInstance.create(FormService::class.java)
}