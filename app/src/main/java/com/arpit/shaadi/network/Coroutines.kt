package com.arpit.shaadi.network

import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.data.model.ServerResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.arpit.shaadi.data.model.Result
import com.arpit.shaadi.repository.HomeDataRepository

val ioScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
val mainScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

/**
 *
 * Fetch the list of cards.
 *
 */

fun fetchCards(
    block: (response: Result<ServerResponse<ShaadiCards>>?) -> Unit
) {
    ioScope.launch {
        val response = HomeDataRepository(
            NetworkModule.formService
        ).getCards()
        block(response)
    }
}