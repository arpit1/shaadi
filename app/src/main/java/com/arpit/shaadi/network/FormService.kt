package com.arpit.shaadi.network

import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.data.model.ServerResponse
import retrofit2.Response
import retrofit2.http.*

interface FormService {
    @GET(".")
    suspend fun getCards(@Query("results") result: Int): Response<ServerResponse<ShaadiCards>>
}