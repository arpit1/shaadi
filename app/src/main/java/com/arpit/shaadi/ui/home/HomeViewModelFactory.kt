package com.arpit.shaadi.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.shaadi.data.db.repository.ShaadiCardRepository
import com.arpit.shaadi.data.preferences.PreferenceProvider

class HomeViewModelFactory(
    private val roomDbRepository: ShaadiCardRepository,
    private val prefs: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(
            roomDbRepository,
            prefs
        ) as T
    }
}