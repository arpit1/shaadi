package com.arpit.shaadi.ui.home

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.arpit.shaadi.common.BaseViewModel
import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.data.db.repository.ShaadiCardRepository
import com.arpit.shaadi.data.model.Result
import com.arpit.shaadi.util.isInternetAvailable
import com.arpit.shaadi.data.model.ErrorModel
import com.arpit.shaadi.data.preferences.PreferenceProvider
import com.arpit.shaadi.network.fetchCards
import com.arpit.shaadi.network.mainScope
import com.arpit.shaadi.notifiers.Notify
import com.arpit.shaadi.util.Constant.Companion.NO_INTERNET
import com.arpit.shaadi.util.Constant.Companion.NO_INTERNET_CONNECTION_ERROR
import com.arpit.shaadi.util.Constant.Companion.ON_ERROR
import com.arpit.shaadi.util.Constant.Companion.RETRY
import com.arpit.shaadi.util.showErrorModel
import kotlinx.coroutines.launch

class HomeViewModel(
    private val roomDbRepository: ShaadiCardRepository,
    private val prefs: PreferenceProvider
) : BaseViewModel() {

    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)

    var cardsList: MutableLiveData<List<ShaadiCards>> = MutableLiveData()
    var arrCards: ArrayList<ShaadiCards> = arrayListOf()

    fun getUserShaadiCard() {
        if (isInternetAvailable()) {
            showProgress()
            fetchCards { result ->
                when (result) {
                    is Result.Success -> {
                        onError.set(false)
                        hideProgress()
                        if (result.data.results!!.isNotEmpty()) {
                            prefs.saveData(DATA_SAVED, true)
                            mainScope.launch {
                                roomDbRepository.saveRepo(result.data.results!!)
                            }
                            modifyDataAsPerStatus(result.data.results!!)
                        }
                    }
                    is Result.Error -> {
                        onError.set(true)
                        hideProgress()
                        result.exception.printStackTrace()
                    }
                    else -> {
                        onError.set(true)
                        hideProgress()
                    }
                }
            }
        } else {
            if (prefs.getData(DATA_SAVED)!!) {
                mainScope.launch {
                    modifyDataAsPerStatus(roomDbRepository.getRepo())
                }
            } else {
                onError.set(true)
                errorModel.showErrorModel(NO_INTERNET, NO_INTERNET_CONNECTION_ERROR, RETRY) {
                    getUserShaadiCard()
                }
            }
        }
    }

    private fun modifyDataAsPerStatus(arr: List<ShaadiCards>) {
        val jsonArr = getJsonFromPrefs()
        arr.forEach {
            if (jsonArr?.contains("${it.email}~true")!!) {
                it.status = true
                it.accepted = true
                it.rejected = false
            } else if (jsonArr.contains("${it.email}~false")) {
                it.status = true
                it.accepted = false
                it.rejected = true
            }
        }
        cardsList.postValue(arr)
    }

    fun updateStatus(status: Boolean, email: String) {
        val arr = getJsonFromPrefs()
        arr?.add("$email~$status")
        prefs.saveCardsJsonObject(arr!!)
        var pos = -1

        arrCards = ArrayList(cardsList.value!!)

        run loop@{
            arrCards.forEach {
                pos++
                if (it.email == email) {
                    it.status = true
                    if (status) {
                        it.accepted = true
                        it.rejected = false
                    } else {
                        it.accepted = false
                        it.rejected = true
                    }
                    return@loop
                }
            }
        }

        notifier.notify(Notify(ITEM_CHANGED, pos, arrCards))
    }

    private fun getJsonFromPrefs(): ArrayList<String?>? {
        var arr = prefs.getCardsJsonObject()
        if (arr == null)
            arr = ArrayList()

        return arr
    }

    companion object {
        const val DATA_SAVED = "dataSaved"
        const val ITEM_CHANGED = "itemChanged"
    }
}