package com.arpit.shaadi.ui.home

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.arpit.shaadi.R
import com.arpit.shaadi.adapter.ShaadiUsersListAdapter
import com.arpit.shaadi.common.BaseActivity
import com.arpit.shaadi.common.BaseViewModel
import com.arpit.shaadi.data.db.entities.ShaadiCards
import com.arpit.shaadi.databinding.ActivityMainBinding
import com.arpit.shaadi.notifiers.Notify
import com.arpit.shaadi.ui.home.HomeViewModel.Companion.ITEM_CHANGED
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class HomeActivity : BaseActivity() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var toolbar: Toolbar
    private val binding: ActivityMainBinding by lazyBinding()

    private val factory by instance<HomeViewModelFactory>()

    override val dataBinding: Boolean = true
    override val layoutResource: Int = R.layout.activity_main
    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun initializeViewModel() {
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
    }

    override fun setBindings() {
        binding.viewModel = viewModel
        binding.errorModel = viewModel.errorModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbar()
        setObserver()
        initRecyclerView()

        viewModel.getUserShaadiCard()
    }

    private fun setObserver() {
        viewModel.cardsList.observe(this, Observer {
            if (it is ArrayList<*>) {
                val adapter = (recyclerView.adapter) as ShaadiUsersListAdapter
                adapter.cards = ArrayList(viewModel.cardsList.value!!)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun initRecyclerView() {
        recyclerView.also {
            it.layoutManager = LinearLayoutManager(this)
            it.setHasFixedSize(true)
            it.adapter = ShaadiUsersListAdapter(ArrayList(), viewModel)
            val animator: RecyclerView.ItemAnimator = it.itemAnimator!!

            if (animator is SimpleItemAnimator) {
                animator.supportsChangeAnimations = false
            }
        }
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)

        toolbar.title = resources.getString(R.string.app_name)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
//        toolbar.setNavigationIcon(R.drawable.back_arrow_white)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item!!)
    }

    override fun onNotificationReceived(data: Notify) {
        when (data.identifier) {
            ITEM_CHANGED -> {
                val adapter = (recyclerView.adapter) as ShaadiUsersListAdapter
                adapter.cards = data.arguments[1] as ArrayList<ShaadiCards>
                adapter.notifyItemChanged(data.arguments[0] as Int)
            }
        }
    }
}